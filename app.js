var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var register = require('./register');
var _ = require('underscore');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express['static'](path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

app.post('/unregisterAll', function(req, res, next)
{
    var body = req.body || {};
    register.stopAll(body.targetIP, function(err)
    {
        if(err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
});

app.post('/unregister', function(req, res, next)
{
    var body = req.body || {};
    register.stop(body.username, body.targetIP, function(err)
    {
        if(err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
});

app.post('/register', function(req, res, next)
{
    var body = req.body || {};
    register.start(body.username, body.targetIP, body.targetPort, body.domain, body.stationPort, body.expire, function(err)
    {
        if(err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
