var sip = require('sip');
var os = require('os');
var _ = require('underscore');
var clone = require('clone');
var dgram = require('dgram');
var packInfo = require('./package.json');

var ip;
_.each(os.networkInterfaces(), function(iface)
{
    _.each(iface, function(addr)
    {
        if(!addr.internal && addr.family === 'IPv4')
            ip = addr.address;
    });
});

var currentReg = {};

function randomString(iLength, sAlpha)
{
    if(iLength === undefined) iLength = 8;
    if(sAlpha === undefined) sAlpha = "all";
    var string = "";
    var min, max, reg;
    switch(sAlpha)
    {
        case 'all':
            min = 33;
            max = 126;
            reg = '.';
        break;
        case 'lower':
            min = 97;
            max = 122;
            reg = '[a-z]';
        break;
        case 'upper':
            min = 65;
            max = 90;
            reg = '[A-Z]';
        break;
        case 'alpha':
            min = 65;
            max = 122;
            reg = '[a-zA-Z]';
        break;
        case 'lowernum':
            min = 48;
            max = 122;
            reg = '[a-z0-9]';
        break;
        case 'uppernum':
            min = 48;
            max = 90;
            reg = '[A-Z0-9]';
        break;
        case 'alphanum':
            min = 48;
            max = 122;
            reg = '[A-Za-z0-9]';
        break;
        case 'numeric':
            min = 48;
            max = 57;
            reg = '[0-9]';
        break;
        case 'hex':
            min = 48;
            max = 70;
            reg = '[A-F0-9]';
        break;
        default:
            min = 33;
            max = 126;
            reg = sAlpha;
        break;
    }

    while(string.length < iLength)
    {
        var character = String.fromCharCode(Math.floor(Math.random() * (max - min + 1)) + min);
        if(character.search(reg) === 0) string += character;
    }
    return string;
}

exports.stopAll = function(targetIP, cb)
{
    if(_.isObject(currentReg[targetIP]))
    {
        _.each(currentReg[targetIP], function(reg, username)
        {
            sendRegistrationWrapper(username, targetIP, reg.targetPort, reg.domain, reg.stationPort, 0);
            if(_.isObject(currentReg[targetIP][username].intervalID))
            {
                clearInterval(currentReg[targetIP][username].intervalID);
            }
        });
        delete currentReg[targetIP];
        cb();
    }
    else
    {
        cb(new Error("No registrations for "+targetIP));
    }
};

exports.stop = function(username, targetIP, cb)
{
    if(username === undefined)
    {
        cb(new Error("Username must be defined"));
        return;
    }

    if(targetIP === undefined)
    {
        cb(new Error("Target IP must be defined"));
        return;
    }

    if(_.isObject(currentReg[targetIP]) && _.isObject(currentReg[targetIP][username]))
    {
        sendRegistrationWrapper(username, targetIP, currentReg[targetIP][username].targetPort, currentReg[targetIP][username].domain, currentReg[targetIP][username].stationPort, 0);
        if(_.isObject(currentReg[targetIP][username].intervalID))
        {
            clearInterval(currentReg[targetIP][username].intervalID);
            delete currentReg[targetIP][username];
        }
        cb();
    }
    else
    {
        cb(new Error("Registration not found"));
    }
};

exports.start = function(username, targetIP, targetPort, domain, stationPort, expire, cb)
{
    if(username === undefined)
    {
        cb(new Error("Username must be defined"));
        return;
    }

    if(targetIP === undefined)
    {
        cb(new Error("Target IP must be defined"));
        return;
    }

    targetPort = targetPort   || 5060;
    domain = domain           || 'Qfun.com';
    stationPort = stationPort || 8060;
    expire = expire           || 3600;
    var intervalSec = Math.floor(expire * 0.75);
    currentReg[targetIP] = currentReg[targetIP] || {};
    currentReg[targetIP][username] = currentReg[targetIP][username] || {};
    if(_.isObject(currentReg[targetIP][username].intervalID))
    {
        clearInterval(currentReg[targetIP][username].intervalID);
        delete currentReg[targetIP][username];
    }
    var intFunc = function()
    {
        sendRegistrationWrapper(username, targetIP, targetPort, domain, stationPort, expire);
    };
    intFunc();
    currentReg[targetIP][username].intervalID  = setInterval(intFunc, intervalSec*1000);
    currentReg[targetIP][username].targetPort  = targetPort;
    currentReg[targetIP][username].domain      = domain;
    currentReg[targetIP][username].stationPort = stationPort;
    currentReg[targetIP][username].expire      = expire;
    cb();
};

function sendRegistrationWrapper(username, targetIP, targetPort, domain, stationPort, expire)
{
    var callid = randomString(32, 'hex');
    var seq = Math.floor(Math.random()*1000);
    sendRegistration(username, targetIP, targetPort, domain, stationPort, expire, callid, seq);
    seq++;
    sendRegistration(username, targetIP, targetPort, domain, stationPort, expire, callid, seq);
}

function sendRegistration(username, targetIP, targetPort, domain, stationPort, expire, callid, seq)
{
    var template = {
        "method": "REGISTER",
        "version": "2.0",
        "headers": {
            "cseq": {
                "method": "REGISTER"
            },
            "max-forwards": "70",
            "via": [
                {
                    "version": "2.0",
                    "protocol": "UDP"
                }
            ],
            "supported": "join, replaces",
            "user-agent": packInfo.name+"/"+packInfo.version,
            "content-length": 0
        },
        "content": ""
    };

    var regObj = clone(template);

    regObj.uri = "sip:"+targetIP+":"+targetPort;
    regObj.headers.to = {};
    regObj.headers.to.uri = "sip:"+username+"@"+domain;
    regObj.headers.to.params = {};
    regObj.headers.from = {};
    regObj.headers.from.uri = "sip:"+username+"@"+domain;
    regObj.headers.from.params = {tag:randomString(7, 'alphanum')};
    regObj.headers['call-id'] = callid+"@"+ip;
    regObj.headers.cseq.seq = seq;
    var viaObj = _.first(regObj.headers.via);
    viaObj.host = ip;
    viaObj.port = stationPort;
    viaObj.params = {branch:randomString(27, 'alphanum')};
    var contactObj = {};
    contactObj.uri = "sip:"+username+"@"+ip+":"+stationPort;
    contactObj.params = {};
    regObj.headers.contact = [contactObj];
    regObj.headers.expires = expire;

    console.log(sip.stringify(regObj));
    var messBuff = new Buffer(sip.stringify(regObj));
    var client = dgram.createSocket("udp4");
    client.send(messBuff, 0, messBuff.length, targetPort, targetIP, function(err, bytes)
    {
        client.close();
    });
}

